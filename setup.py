from setuptools import setup, find_packages

setup(
      name='memoria',
      version='1.0',
      description='Tools for saving memory, encryption, and password management. Listen to this: https://youtu.be/vabnZ9-ex7o ',
      url='',
      author='Idin',
      author_email='d@idin.net',
      license='',
      packages=find_packages(exclude=("jupyter_tests", ".idea", ".git")),
      install_requires=['send2trash', 'pathlib', 'base32hex', 'datetime', 'aenum', 'dill'],
      python_requires='~=3.5',
      zip_safe=False
)