from .Box import Box
from slytherin.time import Timer
from slytherin.progress import ProgressBar
from .hash_collection import make_hash_sha256
import time

from .CacheObj import CacheObj


class Cache:
	_ENCODING_BASE = 64

	def __init__(self, path='cache', days_to_expire = 30, extension='memoria'):
		self._box = Box(path=path, extension=extension)

		self._days_to_expire = days_to_expire
		self._timer = Timer()

	@property
	def path(self):
		return self._box.path

	def save(self, echo=True):
		self._box.save(echo=echo)

	@classmethod
	def hash(cls, obj):
		return make_hash_sha256(obj=obj, base=cls._ENCODING_BASE)


	@property
	def cache_keys(self):
		return self._box.names

	@property
	def size(self):
		return self._box.size

	@property
	def file_size(self):
		return self._box.file_size

	def get(self, cache_key):
		return self._box.get(name=cache_key)

	def is_expired(self, cache_key):
		cached_version = self._box.get(name=cache_key)
		elapsed = cached_version.timer.get_elapsed()
		return elapsed.days >= self._days_to_expire

	def remove_expired(self, progress_bar=True):
		cache_keys = list(self.cache_keys)
		actual_progress_bar = ProgressBar(total=self.size)
		progress = 0
		for cache_key in cache_keys:
			if self.is_expired(cache_key=cache_key):
				self.remove(cache_key=cache_key)
			if progress_bar:
				progress+=1
				actual_progress_bar.show(amount=progress)

	def to_list(self):
		the_list = [cache_obj.get_complete_dict() for key, cache_obj in self._box.items]
		the_list.sort(key=lambda x:x['_time_'])
		return the_list

	def remove(self, cache_key=None):
		self._box.remove(name=cache_key)

	# takes the n cached objects from the beginning
	def remove_earliest(self, n=1):
		n = min(n, self.size)
		to_remove = self.to_list()[0:n]
		for cache_dict in to_remove:
			self.remove(cache_key=cache_dict['_cache_key_'])
		return to_remove

	def flush(self):
		self._box.flush()

	def contains(self, cache_key):
		return self._box.contains(name=cache_key)

	def cache(self, func, condition_func=lambda x:True, func_name=None, echo=False, hashed_args=None, unhashed_args=None, use_cache=True, num_tries=1, delay=0):
		# the arguments can either be passed through a dictionary or kwargs
		"""
		:param return_cache_key: if True, the cache key is also returned with the result
		:type func: callable
		:type condition_func: callable
		:param condition_func: a function that determines if the results are OK to be saved or not
		:type echo: bool
		:type hashed_args: dict or NoneType
		:type unhashed_args: dict or NoneType
		:return: result of func(**args_dict, **kwargs)
		"""
		cache_obj = CacheObj(hashed_args=hashed_args, unhashed_args=unhashed_args, func_name=func_name)

		# from cache
		if self._box.contains(cache_obj.cache_key) and use_cache:
			cached_version = self._box.get(cache_obj.cache_key)
			if cached_version.elapsed().days<self._days_to_expire:
				if echo: print(f'using the cached version for: {hashed_args}')
				return cached_version

		# new run
		for i in range(num_tries):
			cache_obj.run(func=func)
			if condition_func(cache_obj.result):
				if echo: print(f'caching result of: {hashed_args}')
				self._box.put(name=cache_obj.cache_key, obj=cache_obj)
				return cache_obj
			time.sleep(2**i*delay)

		if echo: print(f'result of: {hashed_args} is not cachable.')
		return cache_obj


