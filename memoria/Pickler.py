import dill
import pickle
import send2trash

class Pickler:
	@staticmethod
	def save(path, obj, echo=0):
		echo = max(0, echo)
		with open(file=path, mode='wb') as output:
			try:
				dill.dump(obj=obj, file=output, protocol=dill.HIGHEST_PROTOCOL)
				if echo: print(f'dilled {path}')
			except:
				pickle.dump(obj=obj, file=output, protocol=pickle.HIGHEST_PROTOCOL)
				if echo: print(f'pickled {path}')

	write = save
	put = save
	dump = save
	pickle = save

	@staticmethod
	def load(path, echo=0):
		echo = max(0, echo)
		with open(file=path, mode='rb') as input:
			try:
				obj = dill.load(file=input)
				if echo: print(f'undilled {path}')
			except:
				obj = pickle.load(file=input)
				if echo: print(f'unpickled {path}')
		return obj

	read = load
	get = load

	@staticmethod
	def delete(path):
		send2trash.send2trash(path)