import dill as pickle
import atexit
from pathlib import Path
import send2trash
import os

class Box:
	def __init__(self, path='box', extension='memoria'):
		if not path.endswith(f".{extension}"):
			path += f".{extension}"
		self._path = path
		dir_name = os.path.dirname(path)
		if dir_name != '':
			os.makedirs(dir_name, exist_ok=True)

		if self.file_exists():
			self._dict = pickle.load(file=open(file=self._path, mode='rb'))
		else:
			self._dict = {}
		atexit.register(self.save)

	@property
	def path(self):
		return self._path

	@property
	def names(self):
		return self._dict.keys()

	@property
	def items(self):
		return self._dict.items()

	@property
	def file_size(self):
		try:
			return os.path.getsize(self.path)
		except:
			return 0

	def get_all_names(self):
		return self._dict.keys()

	@property
	def size(self):
		return len(self._dict)

	def file_exists(self):
		return Path(self._path).is_file()

	def save(self, echo=True):
		if len(self._dict)>0:
			if echo: print(f'Saving to "{self._path}"')
			pickle.dump(obj=self._dict, file=open(file=self._path, mode='wb'))
		else:
			if echo: print('Nothing to save!')
			if self.file_exists():
				send2trash.send2trash(self._path)

	def contains(self, name):
		return name in self._dict

	def get(self, name):
		return self._dict[name]



	def put(self, name, obj):
		self._dict[name] = obj

	def remove(self, name):
		del self._dict[name]

	def flush(self):
		self._dict = {}
