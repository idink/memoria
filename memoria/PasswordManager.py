from .Vault import Vault
import getpass

class PasswordManager(Vault):
	def __init__(self, path='C:\\code\\memoria\\passwords.memoria', key='memoria'):
		super().__init__(path=path, key=key, timeout=None)

	_UNLOCK_MESSAGE = 'Please enter the Password Manager key:'

	def get_password(self, x):
		if self.contains(name=x):
			return self.get(name=x)
		else:
			print(f'You have not entered the password for {x}. Please enter it:')
			password = getpass.getpass()
			self.put(name=x, obj=password)
			return password

	def update_password(self, x):
		password = getpass.getpass()
		self.put(name=x, obj=password)


